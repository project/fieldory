<?php

namespace Drupal\fieldory\Plugin\Fieldory;

use Drupal\fieldory\FieldoryBase;
use Drupal\fieldory\FrequentlyUsedOptions\IntegerFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptionsBase;

/**
 * Class IntegerField.
 *
 * @Fieldory(
 *     id = "integer",
 *     description = "Fieldory for integer field",
 * )
 */
class IntegerField extends FieldoryBase {

  /**
   * The field type.
   *
   * @var string
   */
  protected $fieldType = 'integer';

  /**
   * StringField constructor.
   */
  public function __construct() {
    $this->setFieldFormOptions(['type' => 'number']);
    $this->setFieldViewOptions(['type' => 'number_integer']);
  }

  /**
   * Set frequently used options.
   *
   * @inheritDoc
   */
  public function setFrequentlyUsedOptions(FrequentlyUsedOptionsBase $options) {
    if ($options instanceof IntegerFrequentlyUsedOptions) {
      $this->setFieldSettings([
        'min' => $options->min,
        'max' => $options->max,
        'prefix' => $options->prefix,
        'suffix' => $options->suffix,
      ]);
    }
    return $this;
  }

}
