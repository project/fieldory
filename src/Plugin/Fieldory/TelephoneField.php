<?php

namespace Drupal\fieldory\Plugin\Fieldory;

use Drupal\fieldory\FieldoryBase;

/**
 * Class TelephoneField.
 *
 * @Fieldory(
 *     id = "telephone",
 *     description = "Fieldory for telephone field",
 * )
 */
class TelephoneField extends FieldoryBase {

  /**
   * The field type.
   *
   * @var string
   */
  protected $fieldType = 'telephone';

  /**
   * {@inheritDoc}
   */
  protected $requiredModules = ['telephone'];

  /**
   * StringField constructor.
   */
  public function __construct() {
    $this->setFieldFormOptions(['type' => 'telephone_default']);
    $this->setFieldViewOptions(['type' => 'basic_string']);
  }

}
