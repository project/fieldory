<?php

namespace Drupal\fieldory\Plugin\Fieldory;

use Drupal\fieldory\FieldoryBase;
use Drupal\fieldory\FrequentlyUsedOptions\BoolFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptions\TermFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptionsBase;

/**
 * Class TermField.
 *
 * @Fieldory(
 *     id = "term",
 *     description = "Fieldory for term field",
 * )
 */
class TermField extends FieldoryBase {

  /**
   * The field type.
   *
   * @var string
   */
  protected $fieldType = 'entity_reference';

  /**
   * BoolField constructor.
   */
  public function __construct() {
    $this->setFieldStorageSettings(['target_type' => 'taxonomy_term']);
    $this->setFieldFormOptions(['type' => 'options_select']);
    $this->setFieldViewOptions(['type' => 'entity_reference_label']);
  }

  /**
   * Set frequently used options.
   *
   * @inheritDoc
   */
  public function setFrequentlyUsedOptions(FrequentlyUsedOptionsBase $options) {
    if ($options instanceof TermFrequentlyUsedOptions) {
      $this->setFieldSettings([
        'handler_settings' => [
          'target_bundles' => [
            $options->vid => $options->vid,
          ],
          'auto_create' => TRUE,
        ],
      ]);
    }
    return $this;
  }

}
