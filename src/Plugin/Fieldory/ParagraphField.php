<?php

namespace Drupal\fieldory\Plugin\Fieldory;

use Drupal\fieldory\FieldoryBase;
use Drupal\fieldory\FrequentlyUsedOptions\ParagraphFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptions\TermFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptionsBase;

/**
 * Class ParagraphField.
 *
 * @Fieldory(
 *     id = "paragraph",
 *     description = "Fieldory for paragraph field",
 * )
 */
class ParagraphField extends FieldoryBase {

  /**
   * The field type.
   *
   * @var string
   */
  protected $fieldType = 'entity_reference_revisions';

  /**
   * {@inheritDoc}
   */
  protected $requiredModules = ['paragraphs'];

  /**
   * BoolField constructor.
   */
  public function __construct() {
    $this->setFieldStorageSettings(['target_type' => 'paragraph']);
    $this->setFieldFormOptions(['type' => 'entity_reference_paragraphs']);
    $this->setFieldViewOptions(['type' => 'entity_reference_revisions_entity_view']);
  }

  /**
   * Set frequently used options.
   *
   * @inheritDoc
   */
  public function setFrequentlyUsedOptions(FrequentlyUsedOptionsBase $options) {
    if ($options instanceof ParagraphFrequentlyUsedOptions) {
      $paragraph_bundle_name = $options->paragraph_bundle_name;
      $this->setFieldSettings([
        'handler' => 'default:paragraph',
        'handler_settings' => [
          'negate' => 0,
          'target_bundles' => [
            $paragraph_bundle_name => $paragraph_bundle_name,
          ],
          'target_bundles_drag_drop' => [
            $paragraph_bundle_name => [
              'enabled' => TRUE,
            ],
          ],
        ],
      ]);
    }
    return $this;
  }

}
