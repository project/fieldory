<?php

namespace Drupal\fieldory\Plugin\Fieldory;

use Drupal\fieldory\FieldoryBase;

/**
 * Class TextLongField.
 *
 * @Fieldory(
 *     id = "text_long",
 *     description = "Fieldory for text long field",
 * )
 */
class TextLongField extends FieldoryBase {

  /**
   * The field type.
   *
   * @var string
   */
  protected $fieldType = 'text_long';

  /**
   * StringField constructor.
   */
  public function __construct() {
    $this->setFieldFormOptions(['type' => 'text_textarea']);
    $this->setFieldViewOptions(['type' => 'text_default']);
  }

}
