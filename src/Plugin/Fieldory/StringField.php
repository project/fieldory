<?php

namespace Drupal\fieldory\Plugin\Fieldory;

use Drupal\fieldory\FieldoryBase;

/**
 * Class StringField.
 *
 * @Fieldory(
 *     id = "string",
 *     description = "Fieldory for string field",
 * )
 */
class StringField extends FieldoryBase {

  /**
   * The field type.
   *
   * @var string
   */
  protected $fieldType = 'string';

  /**
   * StringField constructor.
   */
  public function __construct() {
    $this->setFieldFormOptions(['type' => 'string_textfield']);
    $this->setFieldViewOptions(['type' => 'string']);
  }

}
