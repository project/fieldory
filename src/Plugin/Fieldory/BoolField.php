<?php

namespace Drupal\fieldory\Plugin\Fieldory;

use Drupal\fieldory\FieldoryBase;
use Drupal\fieldory\FrequentlyUsedOptions\BoolFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptionsBase;

/**
 * Class BoolField.
 *
 * @Fieldory(
 *     id = "bool",
 *     description = "Fieldory for bool field",
 * )
 */
class BoolField extends FieldoryBase {

  /**
   * The field type.
   *
   * @var string
   */
  protected $fieldType = 'boolean';

  /**
   * BoolField constructor.
   */
  public function __construct() {
    $this->setFieldSettings([
      'on_label' => 'Yes',
      'off_label' => 'No',
    ]);
    $this->setFieldFormOptions(['type' => 'boolean_checkbox']);
    $this->setFieldViewOptions(['type' => 'boolean']);
  }

  /**
   * Set frequently used options.
   *
   * @inheritDoc
   */
  public function setFrequentlyUsedOptions(FrequentlyUsedOptionsBase $options) {
    if ($options instanceof BoolFrequentlyUsedOptions) {
      $this->setFieldSettings([
        'on_label' => $options->onLabel,
        'off_label' => $options->offLabel,
      ]);
    }
    return $this;
  }

}
