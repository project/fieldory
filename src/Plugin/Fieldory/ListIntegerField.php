<?php

namespace Drupal\fieldory\Plugin\Fieldory;

use Drupal\fieldory\FieldoryBase;
use Drupal\fieldory\FrequentlyUsedOptions\ListIntegerFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptionsBase;

/**
 * Class ListIntegerField.
 *
 * @Fieldory(
 *     id = "list_integer",
 *     description = "Fieldory for list_integer field",
 * )
 */
class ListIntegerField extends FieldoryBase {

  /**
   * The field type.
   *
   * @var string
   */
  protected $fieldType = 'list_integer';

  /**
   * StringField constructor.
   */
  public function __construct() {
    $this->setFieldFormOptions(['type' => 'options_select']);
    $this->setFieldViewOptions(['type' => 'list_default']);
  }


  /**
   * Set frequently used options.
   *
   * @inheritDoc
   */
  public function setFrequentlyUsedOptions(FrequentlyUsedOptionsBase $options) {
    if ($options instanceof ListIntegerFrequentlyUsedOptions) {
      $this->setFieldStorageSettings([
        'allowed_values' => $options->options,
        'allowed_values_function' => '',
      ]);
      if ($options->useWidgetOptionsButtons) {
        $form_options = $this->getFieldFormOptions();
        $form_options['type'] = 'options_buttons';
        $this->setFieldFormOptions($form_options);
      }
    }
    return $this;
  }

}
