<?php

namespace Drupal\fieldory\Plugin\Fieldory;

use Drupal\fieldory\FieldoryBase;
use Drupal\fieldory\FrequentlyUsedOptions\FloatFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptionsBase;

/**
 * Class FloatField.
 *
 * @Fieldory(
 *     id = "float",
 *     description = "Fieldory for float field",
 * )
 */
class FloatField extends FieldoryBase {

  /**
   * The field type.
   *
   * @var string
   */
  protected $fieldType = 'float';

  /**
   * StringField constructor.
   */
  public function __construct() {
    $this->setFieldFormOptions(['type' => 'number']);
    $this->setFieldViewOptions(['type' => 'number_unformatted']);
  }

  /**
   * Set frequently used options.
   *
   * @inheritDoc
   */
  public function setFrequentlyUsedOptions(FrequentlyUsedOptionsBase $options) {
    if ($options instanceof FloatFrequentlyUsedOptions) {
      $this->setFieldSettings([
        'min' => $options->min,
        'max' => $options->max,
        'prefix' => $options->prefix,
        'suffix' => $options->suffix,
      ]);
    }
    return $this;
  }

}
