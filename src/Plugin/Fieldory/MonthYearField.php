<?php

namespace Drupal\fieldory\Plugin\Fieldory;

use Drupal\fieldory\FieldoryBase;

/**
 * Class MonthYearField.
 *
 * @Fieldory(
 *     id = "month_year",
 *     description = "Fieldory for month_year field",
 * )
 */
class MonthYearField extends FieldoryBase {

  /**
   * The field type.
   *
   * @var string
   */
  protected $fieldType = 'month_year';

  /**
   * {@inheritDoc}
   */
  protected $requiredModules = ['month_year'];

  /**
   * StringField constructor.
   */
  public function __construct() {
    $this->setFieldFormOptions([
      'type' => 'month_year_widget',
      'settings' => [
        'min_year' => 1900,
        'adjustment' => 0,
        'disabled_months' => [],
      ],
    ]);
    $this->setFieldViewOptions(['type' => 'month_year_formatter']);
  }

}
