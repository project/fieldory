<?php

namespace Drupal\fieldory;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\fieldory\Annotation\Fieldory;

/**
 * Class FieldoryFieldManager.
 *
 * @package Drupal\fieldory
 */
class FieldoryManager extends DefaultPluginManager {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * FieldoryManger constructor.
   *
   * @param \Traversable $namespaces
   *   The namespaces.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct('Plugin/Fieldory', $namespaces, $module_handler, FieldoryInterface::class, Fieldory::class);
    $this->configFactory = $config_factory;
  }

  /**
   * Get the field fieldory instance.
   *
   * @param string $id
   *   The fieldory plugin id.
   *
   * @return \Drupal\fieldory\FieldoryBase
   *   A FieldoryBase instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getFieldoryInstance($id) {
    if (!$this->hasDefinition($id)) {
      throw new PluginNotFoundException($id);
    }
    /** @var \Drupal\fieldory\FieldoryBase $instance */
    $instance = $this->createInstance($id);
    return $instance;
  }

  /**
   * Set field prefix.
   *
   * @param string $prefix
   *   The new prefix to set.
   */
  public function setFieldPrefix($prefix = '') {
    $this->configFactory->getEditable('field_ui.settings')->set('field_prefix', $prefix)->save();
  }

  /**
   * Empty the default field prefix.
   */
  public function emptyFieldPrefix() {
    $this->setFieldPrefix('');
  }

  /**
   * Restore back to the default field prefix.
   */
  public function restoreDefaultFieldPrefix() {
    $this->setFieldPrefix('field_');
  }

}
