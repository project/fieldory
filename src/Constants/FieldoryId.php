<?php

namespace Drupal\fieldory\Constants;

class FieldoryId {
  const STRING = 'string';
  const BOOL = 'bool';
  const LIST_INTEGER = 'list_integer';
  const MONTH_YEAR = 'month_year';
  const INTEGER = 'integer';
  const FLOAT = 'float';
  const TERM = 'term';
  const TELEPHONE = 'telephone';
  const PARAGRAPH = 'paragraph';
  const TEXT_LONG = 'text_long';
}
