<?php

namespace Drupal\fieldory;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\fieldory\Exception\InvalidFieldoryException;
use Drupal\fieldory\Exception\ModuleNotInstalledException;
use Drupal\traits\Traits\MachineNameTrait;

/**
 * Class OptionsBase.
 */
class FieldoryBase implements FieldoryInterface {

  use MachineNameTrait;

  /**
   * The modules required.
   *
   * @var array
   */
  protected $requiredModules = [];

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;


  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type, for example: node.
   *
   * @var string
   */
  protected $entityType = '';

  /**
   * The machine name of bundle.
   *
   * @var string
   */
  protected $bundleName = '';
  /**
   * The label of field.
   *
   * @var string
   */
  protected $fieldLabel = '';
  /**
   * The machine name of field.
   *
   * @var string
   */
  protected $fieldName = '';
  /**
   * The type of field.
   *
   * @var string
   */
  protected $fieldType = '';
  /**
   * Field storage settings.
   *
   * @var array
   */
  protected $fieldStorageSettings = [];
  /**
   * Field instance settings.
   *
   * @var array
   */
  protected $fieldSettings = [];
  /**
   * The machine name of form mode.
   *
   * @var string
   */
  protected $formMode = 'default';
  /**
   * The form display options of the field.
   *
   * @var array
   */
  protected $fieldFormOptions = [];
  /**
   * The machine name of view mode.
   *
   * @var string
   */
  protected $viewMode = 'default';
  /**
   * The view display options of the field.
   *
   * @var array
   */
  protected $fieldViewOptions = [];
  /**
   * The number of filed, unlimited: -1. Default: 1.
   *
   * @var int
   */
  protected $cardinality = 1;
  /**
   * If this is a required field. Default: FALSE.
   *
   * @var bool
   */
  protected $required = FALSE;
  /**
   * Field description.
   *
   * @var string
   */
  protected $fieldDescription = '';

  /**
   * The default value.
   *
   * @var mixed
   */
  protected $fieldDefaultValue;

  /**
   * Set the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * Return the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The instance of entity type manager.
   */
  public function getEntityTypeManager() {
    if ($this->entityTypeManager === NULL) {
      return \Drupal::entityTypeManager();
    }
    return $this->entityTypeManager;
  }

  /**
   * Set The entity type.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return $this
   */
  protected function setEntityType($entity_type) {
    $this->entityType = $entity_type;
    return $this;
  }

  /**
   * Set the field label.
   *
   * @param string $field_label
   *   The field label.
   *
   * @return $this
   */
  protected function setFieldLabel($field_label) {
    $this->fieldLabel = $field_label;
    if (empty($this->fieldName)) {
      $field_prefix = \Drupal::configFactory()->getEditable('field_ui.settings')->get('field_prefix');
      $this->fieldName = $field_prefix . $this->getMachineName($field_label);
    }
    return $this;
  }

  /**
   * Set the bundle name.
   *
   * @inheritDoc
   */
  public function setBundleName($bundle_name) {
    $this->bundleName = $bundle_name;
    return $this;
  }

  /**
   * Set the field name.
   *
   * @inheritDoc
   */
  public function setFieldName($field_name) {
    $this->fieldName = $field_name;
    return $this;
  }

  /**
   * Set the field storage settings.
   *
   * @inheritDoc
   */
  public function setFieldStorageSettings(array $field_storage_settings) {
    $this->fieldStorageSettings = $field_storage_settings;
    return $this;
  }

  /**
   * Set the field settings.
   *
   * @inheritDoc
   */
  public function setFieldSettings(array $field_settings) {
    $this->fieldSettings = $field_settings;
    return $this;
  }

  /**
   * Set the field settings.
   *
   * @inheritDoc
   */
  public function setFormMode($form_mode) {
    $this->formMode = $form_mode;
    return $this;
  }

  /**
   * Set the field form options.
   *
   * @inheritDoc
   */
  public function setFieldFormOptions(array $field_form_options) {
    $this->fieldFormOptions = $field_form_options;
    return $this;
  }

  /**
   * Get the field form options.
   *
   * @inheritDoc
   */
  public function getFieldFormOptions() {
    return $this->fieldFormOptions;
  }

  /**
   * Set the view mode.
   *
   * @inheritDoc
   */
  public function setViewMode($view_mode) {
    $this->viewMode = $view_mode;
    return $this;
  }

  /**
   * Set the field view options.
   *
   * @inheritDoc
   */
  public function setFieldViewOptions(array $field_view_options) {
    $this->fieldViewOptions = $field_view_options;
    return $this;
  }

  /**
   * Set cardinality.
   *
   * @param int $cardinality
   *   The cardinality.
   *
   * @return $this
   */
  public function setCardinality($cardinality) {
    $this->cardinality = $cardinality;
    return $this;
  }

  /**
   * Set the default value.
   *
   * @param mixed $value
   *   The default value.
   *
   * @return $this
   */
  public function setDefaultValue($value) {
    $this->fieldDefaultValue = $value;
    return $this;
  }

  /**
   * Set required.
   *
   * @param bool $required
   *   Required or not.
   *
   * @return $this
   */
  public function setRequired($required) {
    $this->required = $required;
    return $this;
  }

  /**
   * Set the field's form display Weight.
   *
   * @param int $weight
   *   The weight.
   *
   * @return $this
   */
  public function setFormDisplayWeight($weight) {
    $this->fieldFormOptions['weight'] = $weight;
    return $this;
  }

  /**
   * Set the field's view display Weight.
   *
   * @param int $weight
   *   The weight.
   *
   * @return $this
   */
  public function setViewDisplayWeight($weight) {
    $this->fieldViewOptions['weight'] = $weight;
    return $this;
  }

  /**
   * Set description.
   *
   * @param string $description
   *   The description of field.
   *
   * @return $this
   */
  public function setFieldDescription($description) {
    $this->fieldDescription = $description;
    return $this;
  }

  /**
   * Validate properties.
   *
   * @inheritDoc
   */
  public function validate() {
    if (empty($this->entityType)
      || empty($this->bundleName)
      || empty($this->fieldName)
      || empty($this->fieldLabel)
      || empty($this->fieldType)
      || empty($this->formMode)
      || empty($this->viewMode)
      || !is_array($this->fieldStorageSettings)
      || !is_array($this->fieldSettings)
      || !is_array($this->fieldViewOptions)
      || !is_array($this->fieldFormOptions)) {
      throw new InvalidFieldoryException();
    }
    if (!empty($this->requiredModules)) {
      foreach ($this->requiredModules as $module_name) {
        $this->checkModule($module_name);
      }
    }
  }

  /**
   * Run the creation process.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\fieldory\Exception\InvalidFieldoryException
   */
  public function execute() {
    $this->validate();
    $this->createFieldStorage();
    $this->createField();
    $this->setEntityViewDisplay();
    $this->setEntityFormDisplay();
  }

  /**
   * Create field storage.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createFieldStorage() {
    $entity_type = $this->entityType;
    $field_name = $this->fieldName;
    $values = [
      'entity_type' => $entity_type,
      'field_name' => $field_name,
      'type' => $this->fieldType,
      'cardinality' => $this->cardinality,
    ];

    if (!empty($this->fieldStorageSettings)) {
      $values['settings'] = $this->fieldStorageSettings;
    }

    $field_storage = FieldStorageConfig::loadByName($entity_type, $field_name);
    if ($field_storage === NULL) {
      FieldStorageConfig::create($values)->save();
    }
  }

  /**
   * Create field instance.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createField() {

    $entity_type = $this->entityType;
    $bundle_name = $this->bundleName;
    $field_name = $this->fieldName;

    $values = [
      'entity_type' => $entity_type,
      'bundle' => $bundle_name,
      'field_name' => $field_name,
      'label' => $this->fieldLabel,
      'required' => $this->required,
      'description' => $this->fieldDescription,
    ];

    if (!empty($this->fieldSettings)) {
      $values['settings'] = $this->fieldSettings;
    }

    if (!empty($this->fieldDefaultValue)) {
      $values['default_value'] = $this->fieldDefaultValue;
    }

    $field = FieldConfig::loadByName($entity_type, $bundle_name, $field_name);
    if ($field === NULL) {
      FieldConfig::create($values)->save();
    }

  }

  /**
   * Set entity view display.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setEntityViewDisplay() {

    $entity_type = $this->entityType;
    $bundle = $this->bundleName;
    $field_name = $this->fieldName;

    $mode = $this->viewMode;
    $display_options = $this->fieldViewOptions;

    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display */
    $storage = $this->getEntityTypeManager()->getStorage('entity_view_display');

    /* @var \Drupal\Core\Entity\Display\EntityDisplayInterface $display */
    $display = $storage->load($entity_type . '.' . $bundle . '.' . $mode);
    if ($display === NULL) {
      $values = [
        'targetEntityType' => $entity_type,
        'bundle' => $bundle,
        'mode' => $mode,
        'status' => TRUE,
      ];
      $display = $storage->create($values);
      $display->save();
    }
    if (!$display->getComponent($field_name)) {
      if (empty($display_options)) {
        $display->setComponent($field_name);
      }
      else {
        $display->setComponent($field_name, $display_options);
      }
      $display->save();
    }
  }

  /**
   * Set entity form display.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setEntityFormDisplay() {
    $entity_type = $this->entityType;
    $bundle = $this->bundleName;
    $field_name = $this->fieldName;

    $mode = $this->formMode;
    $display_options = $this->fieldFormOptions;

    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $display */
    $storage = $this->getEntityTypeManager()->getStorage('entity_form_display');

    /* @var \Drupal\Core\Entity\Display\EntityDisplayInterface $display */
    $display = $storage->load($entity_type . '.' . $bundle . '.' . $mode);

    if ($display === NULL) {
      $values = [
        'targetEntityType' => $entity_type,
        'bundle' => $bundle,
        'mode' => $mode,
        'status' => TRUE,
      ];
      $display = $storage->create($values);
      $display->save();
    }

    if (!$display->getComponent($field_name)) {
      if (empty($display_options)) {
        $display->setComponent($field_name);
      }
      else {
        $display->setComponent($field_name, $display_options);
      }
      $display->save();
    }

  }

  /**
   * Initialize.
   *
   * @param FieldoryBundle $bundle
   *   The bundle.
   * @param string $field_label
   *   The label of field.
   * @param string $field_name
   *   The field name.
   *
   * @return $this
   */
  public function init(FieldoryBundle $bundle, $field_label, $field_name = '') {
    $this->setEntityType($bundle->entityType);
    $this->setBundleName($bundle->bundleName);
    if (!empty($field_name)) {
      $field_prefix = \Drupal::configFactory()->getEditable('field_ui.settings')->get('field_prefix');
      $this->setFieldName($field_prefix . $field_name);
    }
    $this->setFieldLabel($field_label);
    return $this;
  }

  /**
   * Set frequently used options.
   *
   * @param FrequentlyUsedOptionsBase $options
   *   The frequently used options instance.
   *
   * @return $this
   */
  public function setFrequentlyUsedOptions(FrequentlyUsedOptionsBase $options) {
    return $this;
  }

  public function __destruct() {
    if (PHP_SAPI === 'cli') {
      echo "[Bundle:$this->bundleName] $this->fieldLabel($this->fieldName) created/updated \n";
    }
  }

  /**
   * Set field name by any input.
   *
   * @param string $input
   *   The input.
   *
   * @return $this
   */
  public function setFieldNameByAny($input) {
    $name = $this->getMachineName($input);
    if (!empty($name)) {
      $field_prefix = \Drupal::configFactory()->getEditable('field_ui.settings')->get('field_prefix');
      $this->setFieldName($field_prefix . $name);
    }
    return $this;
  }

  /**
   * The module handler.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected function getModuleHandler() {
    if ($this->moduleHandler === NULL) {
      return \Drupal::moduleHandler();
    }
    return $this->moduleHandler;
  }

  /**
   * Set the module handler.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *
   * @return $this
   */
  protected function setModuleHandler(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
    return $this;
  }

  /**
   * Check if module is installed.
   *
   * @param $module_name
   *
   * @throws \Drupal\fieldory\Exception\ModuleNotInstalledException
   */
  protected function checkModule($module_name) {
    if (!$this->getModuleHandler()->moduleExists($module_name)) {
      throw new ModuleNotInstalledException("The $module_name module is not installed");
    }
  }

}
