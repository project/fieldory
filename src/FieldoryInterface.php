<?php

namespace Drupal\fieldory;

/**
 * Interface FieldoryInterface.
 *
 * @package Drupal\fieldory
 */
interface FieldoryInterface {

  /**
   * Set the bundle name.
   *
   * @param string $bundle_name
   *   The bundle name.
   *
   * @return $this
   */
  public function setBundleName($bundle_name);

  /**
   * Set the field name.
   *
   * @param string $field_name
   *   The field name.
   *
   * @return $this
   */
  public function setFieldName($field_name);

  /**
   * Set the field storage settings.
   *
   * @param array $field_storage_settings
   *   The field storage settings.
   *
   * @return $this
   */
  public function setFieldStorageSettings(array $field_storage_settings);

  /**
   * After init, set common minimal settings.
   *
   * @param array $field_settings
   *   The field settings.
   *
   * @return $this
   */
  public function setFieldSettings(array $field_settings);

  /**
   * Set the form mode.
   *
   * @param string $form_mode
   *   The form mode.
   *
   * @return $this
   */
  public function setFormMode($form_mode);

  /**
   * Set the field form options.
   *
   * @param array $field_form_options
   *   The field form options.
   *
   * @return $this
   */
  public function setFieldFormOptions(array $field_form_options);

  /**
   * Set the view mode.
   *
   * @param string $view_mode
   *   The view mode.
   *
   * @return $this
   */
  public function setViewMode($view_mode);

  /**
   * Set the field view options.
   *
   * @param array $field_view_options
   *   The field view options.
   *
   * @return $this
   */
  public function setFieldViewOptions(array $field_view_options);

  /**
   * Validate properties.
   *
   * @throws \Drupal\fieldory\Exception\InvalidFieldoryException
   */
  public function validate();

}
