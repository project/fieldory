<?php

namespace Drupal\fieldory\FrequentlyUsedOptions;

use Drupal\fieldory\Exception\InvalidFrequentlyUsedOptionsException;
use Drupal\fieldory\FrequentlyUsedOptionsBase;
use Drupal\fieldory\Exception\InvalidPropertyException;

/**
 * Class ListIntegerFrequentlyUsedOptions.
 *
 * @property-read $options The options.
 * @property-read $useWidgetOptionsButtons Flag of using the options_buttons widget.
 */
class ListIntegerFrequentlyUsedOptions extends FrequentlyUsedOptionsBase {

  /**
   * The options array.
   *
   * @var array
   */
  private $options;

  /**
   * To use the options_buttons widget or not.
   *
   * @var bool
   */
  private $useWidgetOptionsButtons;

  /**
   * ListIntegerFrequentlyUsedOptions constructor.
   *
   * @param array $options
   *   The options.
   * @param bool $use_widget_options_buttons
   *   To use the options_buttons widget or not.
   */
  public function __construct($options = [], $use_widget_options_buttons = FALSE) {
    $this->options = $options;
    $this->useWidgetOptionsButtons = $use_widget_options_buttons;
  }

  /**
   * Implements read-only properties.
   *
   * @param string $name
   *   The property name.
   *
   * @return array|bool
   *   The properties.
   *
   * @throws \Drupal\fieldory\Exception\InvalidPropertyException
   * @throws \Drupal\fieldory\Exception\InvalidFrequentlyUsedOptionsException
   */
  public function __get($name) {
    $this->validate();
    if ('options' === $name) {
      return $this->options;
    }
    if ('useWidgetOptionsButtons' === $name) {
      return $this->useWidgetOptionsButtons;
    }
    throw new InvalidPropertyException();
  }

  /**
   * Validate if the option is valid.
   *
   * @throws \Drupal\fieldory\Exception\InvalidFrequentlyUsedOptionsException
   */
  public function validate() {

    if (!is_array($this->options)) {
      throw new InvalidFrequentlyUsedOptionsException();
    }

    if ([] !== $this->options) {
      foreach (array_keys($this->options) as $key) {
        if (!is_numeric($key)) {
          throw new InvalidFrequentlyUsedOptionsException();
        }
      }
    }
  }

}
