<?php

namespace Drupal\fieldory\FrequentlyUsedOptions;

use Drupal\fieldory\FieldoryBundle;
use Drupal\fieldory\FrequentlyUsedOptionsBase;
use Drupal\fieldory\Exception\InvalidPropertyException;

/**
 * Class ParagraphFrequentlyUsedOptions.
 *
 * @property-read $paragraph_bundle_name The paragraph_bundle_name.
 */
class ParagraphFrequentlyUsedOptions extends FrequentlyUsedOptionsBase {

  /**
   * The paragraph_bundle_name.
   *
   * @var int
   */
  private $paragraphBundleName;

  /**
   * ParagraphFrequentlyUsedOptions constructor.
   *
   * @param FieldoryBundle $paragraph_bundle
   */
  public function __construct(FieldoryBundle $paragraph_bundle) {
    $this->paragraphBundleName = $paragraph_bundle->bundleName;
  }

  /**
   * Implements read-only properties.
   *
   * @param string $name
   *   The property name.
   *
   * @return string|false
   *   The label.
   *
   * @throws \Drupal\fieldory\Exception\InvalidPropertyException
   */
  public function __get($name) {
    $this->validate();
    if ('paragraph_bundle_name' === $name) {
      return $this->paragraphBundleName;
    }
    throw new InvalidPropertyException();
  }

}
