<?php

namespace Drupal\fieldory\FrequentlyUsedOptions;

use Drupal\fieldory\FrequentlyUsedOptionsBase;
use Drupal\fieldory\Exception\InvalidPropertyException;

/**
 * Class FloatFrequentlyUsedOptions.
 *
 * @property-read $prefix The prefix.
 * @property-read $suffix The suffix.
 * @property-read $min The min value.
 * @property-read $max The max value.
 */
class FloatFrequentlyUsedOptions extends FrequentlyUsedOptionsBase {

  /**
   * The prefix
   *
   * @var string
   */
  private $prefix;

  /**
   * The suffix.
   *
   * @var string
   */
  private $suffix;

  /**
   * The min value.
   *
   * @var int|null
   */
  private $min;

  /**
   * The max value.
   *
   * @var int|null
   */
  private $max;

  /**
   * IntegerFrequentlyUsedOptions constructor.
   *
   * @param string $prefix
   * @param string $suffix
   * @param int|null $min
   * @param int|null $max
   */
  public function __construct($prefix = '', $suffix = '', $min = NULL, $max = NULL) {
    $this->prefix = $prefix;
    $this->suffix = $suffix;
    $this->min = $min;
    $this->max = $max;
  }

  /**
   * Implements read-only properties.
   *
   * @param string $name
   *   The property name.
   *
   * @return string|false
   *   The label.
   *
   * @throws \Drupal\fieldory\Exception\InvalidPropertyException
   */
  public function __get($name) {
    if ('prefix' === $name) {
      return $this->prefix;
    }
    if ('suffix' === $name) {
      return $this->suffix;
    }
    if ('min' === $name) {
      return $this->min;
    }
    if ('max' === $name) {
      return $this->max;
    }
    throw new InvalidPropertyException();
  }

}
