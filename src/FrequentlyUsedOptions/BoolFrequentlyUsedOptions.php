<?php

namespace Drupal\fieldory\FrequentlyUsedOptions;

use Drupal\fieldory\Exception\InvalidFrequentlyUsedOptionsException;
use Drupal\fieldory\FrequentlyUsedOptionsBase;
use Drupal\fieldory\Exception\InvalidPropertyException;

/**
 * Class BoolFieldOption.
 *
 * @property-read $onLabel The on label.
 * @property-read $offLabel The off label.
 */
class BoolFrequentlyUsedOptions extends FrequentlyUsedOptionsBase {

  /**
   * The on label.
   *
   * @var string
   */
  private $onLabel;
  /**
   * The off label.
   *
   * @var string
   */
  private $offLabel;

  /**
   * BoolFieldOption constructor.
   *
   * @param string $on_label
   *   The on label.
   * @param string $off_label
   *   The off label.
   */
  public function __construct($on_label = 'Yes', $off_label = 'No') {
    $this->onLabel = $on_label;
    $this->offLabel = $off_label;
  }

  /**
   * Implements read-only properties.
   *
   * @param string $name
   *   The property name.
   *
   * @return string|false
   *   The label.
   *
   * @throws \Drupal\fieldory\Exception\InvalidPropertyException
   * @throws \Drupal\fieldory\Exception\InvalidFrequentlyUsedOptionsException
   */
  public function __get($name) {
    $this->validate();
    if ('onLabel' === $name) {
      return $this->onLabel;
    }
    if ('offLabel' === $name) {
      return $this->offLabel;
    }
    throw new InvalidPropertyException();
  }

  /**
   * Validate if the option is valid.
   *
   * @throws \Drupal\fieldory\Exception\InvalidFrequentlyUsedOptionsException
   */
  public function validate() {
    if (empty($this->onLabel) || empty($this->offLabel)) {
      throw new InvalidFrequentlyUsedOptionsException();
    }
  }

}
