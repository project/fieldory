<?php

namespace Drupal\fieldory\FrequentlyUsedOptions;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\fieldory\FrequentlyUsedOptionsBase;
use Drupal\fieldory\Exception\InvalidPropertyException;
use Drupal\traits\Traits\MachineNameTrait;
use \Drupal\traits\Traits\TermTrait;


/**
 * Class TermFrequentlyUsedOptions.
 *
 * @property-read $vid The vid.
 */
class TermFrequentlyUsedOptions extends FrequentlyUsedOptionsBase {

  use TermTrait;
  use MachineNameTrait;

  /**
   * The vocabulary id.
   *
   * @var int
   */
  private $vid;

  /**
   * TermFrequentlyUsedOptions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manger
   *   The entity type manager.
   * @param string $vocabulary_label
   *   The vocabulary label.
   * @param string $vid
   *   The vocabulary id.
   * @param array $terms
   *   The terms.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manger, $vocabulary_label, $vid = '', array $terms = []) {
    if (empty($vid)) {
      $vid = $this->getMachineName($vocabulary_label);
    }
    $this->createVocabulary($entity_type_manger, $vid, $vocabulary_label);
    $this->addTerms($entity_type_manger, $vid, $terms);
    $this->vid = $vid;
  }

  /**
   * Implements read-only properties.
   *
   * @param string $name
   *   The property name.
   *
   * @return string|false
   *   The label.
   *
   * @throws \Drupal\fieldory\Exception\InvalidPropertyException
   */
  public function __get($name) {
    $this->validate();
    if ('vid' === $name) {
      return $this->vid;
    }
    throw new InvalidPropertyException();
  }

}
