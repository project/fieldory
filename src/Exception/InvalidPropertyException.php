<?php

namespace Drupal\fieldory\Exception;

use Exception;

/**
 * Class InvalidPropertyException.
 *
 * @package Drupal\fieldory
 */
class InvalidPropertyException extends Exception {

  /**
   * The error message.
   *
   * @var string
   */
  protected $message = 'Invalid property';

}
