<?php

namespace Drupal\fieldory\Exception;

use Exception;

/**
 * Class ModuleNotInstalledException.
 *
 * @package Drupal\fieldory
 */
class ModuleNotInstalledException extends Exception {

  /**
   * The error message.
   *
   * @var string
   */
  protected $message = 'Required module not installed';

}
