<?php

namespace Drupal\fieldory\Exception;

use Exception;

/**
 * Class InvalidFieldoryException.
 *
 * @package Drupal\fieldory
 */
class InvalidFieldoryException extends Exception {

  /**
   * The error message.
   *
   * @var string
   */
  protected $message = 'Invalid fieldory';

}
