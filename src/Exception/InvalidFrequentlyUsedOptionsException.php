<?php

namespace Drupal\fieldory\Exception;

use Exception;

/**
 * Class InvalidFrequentlyUsedOptionsException.
 *
 * @package Drupal\fieldory
 */
class InvalidFrequentlyUsedOptionsException extends Exception {

  /**
   * The error message.
   *
   * @var string
   */
  protected $message = 'Invalid frequently used options';

}
