<?php

namespace Drupal\fieldory\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class Fieldory.
 *
 * @Annotation
 */
class Fieldory extends Plugin {

  /**
   * The plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * Description of the plugin.
   *
   * @var string
   */
  public $description;

}
