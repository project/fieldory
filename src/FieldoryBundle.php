<?php

namespace Drupal\fieldory;

use Drupal\node\Entity\NodeType;
use Drupal\traits\Traits\MachineNameTrait;

/**
 * Class FieldoryBundle.
 *
 * @property-read $entityType The entity type.
 * @property-read $bundleName The bundle name.
 */
class FieldoryBundle {

  use MachineNameTrait;

  /**
   * The entity type, for example: node.
   *
   * @var string
   */
  private $entityType = '';

  /**
   * The machine name of bundle.
   *
   * @var string
   */
  private $bundleName = '';

  /**
   * Create bundle.
   */
  public function __construct($entity_type_class, $entity_type, $bundle_label, $bundle_name = '') {

    $this->bundleLabel = $bundle_label;
    $this->entityType = $entity_type;

    if (empty($bundle_name)) {
      $bundle_name = $this->getMachineName($bundle_label, $this->randomMachineName());
    }
    $this->bundleName = $bundle_name;

    $bundle = $entity_type_class::load($bundle_name);

    if ($bundle === NULL) {
      $values = [];
      // Node type has different bundle keys.
      if ($entity_type_class === NodeType::class) {
        $values['type'] = $bundle_name;
        $values['name'] = $bundle_label;
      }
      else {
        $values['id'] = $bundle_name;
        $values['label'] = $bundle_label;
      }
      $entity_type_class::create($values)->save();
    }
  }

  /**
   * Implements read-only properties.
   *
   * @param string $name
   *   The property name.
   *
   * @return string|false
   *   The label.
   */
  public function __get($name) {
    if ('entityType' === $name) {
      return $this->entityType;
    }
    if ('bundleName' === $name) {
      return $this->bundleName;
    }
    return FALSE;
  }

}
