<?php

namespace Drupal\fieldory_examples\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\fieldory\Constants\FieldoryId;
use Drupal\fieldory\FieldoryBundle;
use Drupal\fieldory\FieldoryManager;
use Drupal\fieldory\FrequentlyUsedOptions\BoolFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptions\FloatFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptions\IntegerFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptions\ListIntegerFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptions\ParagraphFrequentlyUsedOptions;
use Drupal\fieldory\FrequentlyUsedOptions\TermFrequentlyUsedOptions;
use Drupal\node\Entity\NodeType;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drush\Commands\DrushCommands;

/**
 * Class FieldoryExamplesCommands.
 *
 * @package Drupal\fieldory_examples\Commands
 */
class FieldoryExamplesCommands extends DrushCommands {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The fieldory manager.
   *
   * @var \Drupal\fieldory\FieldoryManager
   */
  private $fieldoryManager;

  /**
   * ApplyToCommands constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\fieldory\FieldoryManager $fieldory_manager
   *   The fieldory manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FieldoryManager $fieldory_manager) {
    parent::__construct();
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldoryManager = $fieldory_manager;
  }

  /**
   * Example usages for the fieldory module.
   *
   * @command fieldory_examples:demo
   * @aliases fedemo
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\fieldory\Exception\InvalidFieldoryException
   */
  public function demo() {

    $bundle = new FieldoryBundle(NodeType::class, 'node', 'Fieldory');

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::STRING)
      ->init($bundle, 'A STRING Field')
      ->execute();

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::TEXT_LONG)
      ->init($bundle, 'A TEXT_LONG Field')
      ->execute();

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::BOOL)
      ->init($bundle, 'A BOOL field')
      ->setFrequentlyUsedOptions(new BoolFrequentlyUsedOptions('Yes', 'No'))
      ->execute();

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::LIST_INTEGER)
      ->init($bundle, 'A LIST_INTEGER field')
      ->setFrequentlyUsedOptions(new ListIntegerFrequentlyUsedOptions(['Male', 'Female']))
      ->execute();

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::LIST_INTEGER)
      ->init($bundle, 'A LIST_INTEGER field 2')
      ->setFrequentlyUsedOptions(new ListIntegerFrequentlyUsedOptions(['Male', 'Female', 'N/A'], TRUE))
      ->execute();

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::LIST_INTEGER)
      ->init($bundle, 'A LIST_INTEGER field 3')
      ->setFrequentlyUsedOptions(new ListIntegerFrequentlyUsedOptions(['Male', 'Female', 'N/A'], TRUE))
      ->setCardinality(-1)
      ->execute();

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::MONTH_YEAR)
      ->init($bundle, 'A MONTH_YEAR field')
      ->execute();

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::INTEGER)
      ->init($bundle, 'A INTEGER field')
      ->setFrequentlyUsedOptions(new IntegerFrequentlyUsedOptions('', 'cm'))
      ->execute();

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::FLOAT)
      ->init($bundle, 'A FLOAT field')
      ->setFrequentlyUsedOptions(new FloatFrequentlyUsedOptions('', 'cm'))
      ->execute();

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::TERM)
      ->init($bundle, 'A TERM field')
      ->setFrequentlyUsedOptions(new TermFrequentlyUsedOptions($this->entityTypeManager, 'Test VOC', '', [
        'Term1',
        'Term2',
      ]))->execute();

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::TELEPHONE)
      ->init($bundle, 'A TELEPHONE Field')
      ->execute();

    // two fields in paragraph.
    $paragraph_bundle = new FieldoryBundle(ParagraphsType::class, 'paragraph', 'A PARAGRAPH BUNDLE');
    // Add fields to paragraph
    $this->fieldoryManager->getFieldoryInstance(FieldoryId::STRING)
      ->init($paragraph_bundle, 'STRING1')
      ->execute();
    $this->fieldoryManager->getFieldoryInstance(FieldoryId::STRING)
      ->init($paragraph_bundle, 'STRING2')
      ->execute();

    $this->fieldoryManager->getFieldoryInstance(FieldoryId::PARAGRAPH)
      ->init($bundle, 'A PARAGRAPH Field')
      ->setFrequentlyUsedOptions(new ParagraphFrequentlyUsedOptions($paragraph_bundle))
      ->execute();

  }

}
