# Fieldory (Field Factory)

- Sick of adding fields one by one by Drupal's built-in field UI?
- Prefer creating fields by coding?

Then, this module is made for you!

Following is the code snippet to create a content type called "Fieldory" with one string field and a long text filed:

```
$fieldory_manager = \Drupal::service('fieldory.fieldory_manager');

$bundle = new FieldoryBundle(NodeType::class, 'node', 'Fieldory');

$fieldory_manager->getFieldoryInstance(FieldoryId::STRING)
  ->init($bundle, 'A STRING Field')
  ->execute();

$fieldory_manager->getFieldoryInstance(FieldoryId::TEXT_LONG)
   ->init($bundle, 'A TEXT_LONG Field')
   ->execute();
```

For more exmples, check out the `Field  (Field Factory) Examples` module. A Drush command called `fieldory_examples:demo` has been added inside it.
